{{-- Extends layout --}}
@extends('layout.default')

{{-- CSS --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: Search Links</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                            <h1 class="font-weight-bold">Search Links</h1>
                        </div>

                    </div>
                    <!-- submit sessions -->
                    <div class="row">
                        <div class="col">
                            @if ($message = session('message'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    {{ $message }}
                                </div>
                            @endif

                            @if ($error = session('error'))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    {{ $error }}
                                </div>
                            @endif

                            @if ($warning = session('warning'))
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    {{ $warning }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row my-5" id="slCategories">
                            @if ($categories->count())
                                @foreach ($categories as $index => $i)
                                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-5">
                                    <div class="categoryCard border-radius-10 p-0">
                                        <a role="button" class="btn btn-block font-weight-boldest font-15" href="{{ route('products.searchlinks.list', [$id, $i->id]) }}">
                                            <img src="{{$i->image}}" width="20" height="20" alt="" class="mr-3">
                                            <span class="font-weight-bold"> {{ $i->title }}</span>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}


@endsection
