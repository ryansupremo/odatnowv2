@extends('layout.default')

@section('styles')
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Admin : Manage Stores : Edit</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <p class="font-weight-bold text-uppercase">Store ID: <span class="text-primary"> {{$store->store_id}}</span></p>
                </div>
                <form action="{{ route('admin.stores.update') }}" method="POST">
                    @csrf
                    <input type="hidden" id="id" name="id" value="{{$store->id}}">
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="name">Store Name</label>
                                <input type="text"
                                    name="name"
                                    id="name"
                                    class="form-control"
                                    value="{{$store->name}}"
                                    required
                                    placeholder="Store Name" >
                                @error('name')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="url">Store URL</label>
                                <input
                                    type="text"
                                    name="url"
                                    id="url"
                                    class="form-control"
                                    value="{{$store->url}}"
                                    required
                                    placeholder="Store URL" >
                                @error('url')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                                @if ($message = session('message'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        {{ $message }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="client_id">Store URLClient ID</label>
                                <input
                                    type="text"
                                    name="client_id"
                                    id="client_id"
                                    class="form-control"
                                    value="{{$store->client_id}}"
                                    required
                                    placeholder="Store Client ID" >
                                @error('client_id')
                                <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="token">Store Token</label>
                                <input
                                    type="text"
                                    name="token"
                                    id="token"
                                    class="form-control"
                                    value="{{$store->token}}"
                                    required
                                    placeholder="Store Token" >
                                @error('token')
                                <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <label >Is Main?</label>
                            <div class="form-group">
                                <label for="is_main_1">Yes
                                    <input id="is_main_1"
                                        type="radio"
                                        name="is_main"
                                        value="1" {{$store->is_main == '1' ? 'checked' :  '' }}>
                                </label>
                                <label for="is_main_2">No
                                    <input id="is_main_2"
                                        type="radio"
                                        name="is_main"
                                        value="0" {{$store->is_main == '0' ? 'checked' : '' }}>
                                </label>
                                @error('is_main')
                                <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                                <a href="{{ route('admin.flags.list') }}" class="btn btn-warning">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
