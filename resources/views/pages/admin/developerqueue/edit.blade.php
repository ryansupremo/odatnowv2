{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Developer Queue : Edit</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif
            <form action="{{ route('admin.developerqueue.update') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $data->id }}">
                <div class="row mt-2">
                    <div class="col-8">
                        <h1>{{ $product->brand_name }}</h1>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="bug">Bug</label>
                        <input type="checkbox" name="bug" id="bug" @if($data->bug == "on") checked @endif>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="new_feature">New Feature</label>
                        <input type="checkbox" name="new_feature" id="new_feature" @if($data->new_feature == "on") checked @endif>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="question">Question</label>
                        <input type="checkbox" name="question" id="question" @if($data->question == "on") checked @endif>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="other">Other</label>
                        <input type="checkbox" name="other" id="other" @if($data->other == "on") checked @endif>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" class="form-control">{{ $data->comments }}</textarea>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required="required">
                            <option selected value="{{ $data->status }}">{{ $data->status }} (Default)</option>
                            <option value="open">open</option>
                            <option value="closed">closed</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" value="Update" class="btn btn-primary">&nbsp;
                        <a href="{{ route('admin.developerqueue.list') }}" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
