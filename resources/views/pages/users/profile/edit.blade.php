{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Edit Profile</h3>
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('profile.update') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="name">Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    value="{{ $user->name }}"
                                    class="form-control"
                                    required="required"
                                    data-lpignore="true"
                                >
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="email">Email</label>
                                <input
                                    type="text"
                                    name="email" id="email" class="form-control"
                                    required="required"
                                    value="{{ $user->email }}"
                                    data-lpignore="true"
                                >
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <div class="alert alert-info">
                                    If you would like to change the password please enter in a new
                                    password or leave the field blank.
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="password">New Password</label>
                                <input
                                    type="password"
                                    name="password"
                                    class="form-control"
                                    id="password"
                                    data-lpignore="true"
                                >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" class="btn btn-success" value="Save">&nbsp;
                        <a href="{{ route('profile.show') }}" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection
