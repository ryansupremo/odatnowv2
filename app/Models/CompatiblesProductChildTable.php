<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompatiblesProductChildTable extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compatibles_product_child_table';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'primary_product_id',
        'child_product_id'
    ];

    public static function countChild($id)
    {
        $data = CompatiblesProductChildTable::from('compatibles_product_child_table as c')
            ->select('c.id')
            ->where('c.primary_product_id', $id)
            ->get();
        return $data;
    }

    public static function getParentInventory()
    {
        $data = CompatiblesProductChildTable::from('compatibles_product_child_table as c')
            ->select(
                'c.id',
                'p.product_id',
                'c.child_product_id',
                'c.primary_product_id',
                'p.inventory_level'
            )
            ->leftJoin('products as p', function($leftJoin)
            {
                $leftJoin->on('p.id', '=', 'c.primary_product_id');
            })
            ->get();
        return $data;
    }
}
