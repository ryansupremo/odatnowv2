<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'category'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * @param $product
     * @param $categories
     * @return bool
     */
    public static function insertCategories($product, $categories)
    {
        for($i = 0; $i < count($categories); $i++){
            $category = new Categories();
            $category->product_id = $product->id;
            $category->category = $categories[$i];
            $category->save();
        }
        return true;
    }

    public static function getCategories($product)
    {
        $data = Categories::from('categories as c')
            ->select(
                'c.product_id',
                'c.category'
            )
            ->where('c.product_id', '=', $product->id)
            ->get()
        ;
        return $data;
    }
}
