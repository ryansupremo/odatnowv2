<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'store_id',
                  'name',
                  'url',
                  'client_id',
                  'token',
                  'is_main'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the store for this model.
     *
     * @return App\Models\Store
     */
    public function store()
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }

    /**
     * Get the client for this model.
     *
     * @return App\Models\Client
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client','client_id');
    }

    public static function getActiveStore()
    {
        $data = Store::from('store as s')
            ->select(
                's.id',
                's.store_id',
                's.name',
                's.url',
                's.client_id',
                's.token'
            )
            ->where('s.is_main', '=', '1')
            ->take(1)
            ->get();
        return $data;
    }

}
