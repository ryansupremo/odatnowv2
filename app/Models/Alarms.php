<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alarms extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'alarms';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'market_check',
        'price_check',
        'seo_check',
        'orders_check',
        'velocity_check',
        'buy_box_review',
        'competitor_check',
        'other',
        'comments',
        'userID',
        'alarm_date'
    ];

    public static function listAlarms($product)
    {
        $result = Alarms::from('alarms as a')
            ->select(
                'a.id',
                'a.market_check',
                'a.price_check',
                'a.seo_check',
                'a.orders_check',
                'a.velocity_check',
                'a.buy_box_review',
                'a.competitor_check',
                'a.other',
                'a.comments',
                'a.alarm_date',
                'u.name'
            )
            ->leftJoin('users as u', function($leftJoin)
            {
                $leftJoin->on('u.id', '=', 'a.userID');
            })
            ->where('a.product_id', $product->id)
            ->take(1)
            ->get()
        ;
        return $result;
    }

    public static function locateAlarms($product)
    {
        $result = Alarms::from('alarms as a')
            ->select('a.id')
            ->where('a.product_id', $product->id)
            ->take(1)
            ->get()
        ;
        return $result;
    }

    public static function alarms($id)
    {
        $count = Alarms::from('alarms as a')
            ->where('a.product_id', '=', $id)
            ->count();
        return $count;
    }
}
