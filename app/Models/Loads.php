<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loads extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'loads';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'source',
                  'box_category_id',
                  'box_sub_category_id',
                  'weight',
                  'box_number',
                  'user_id',
                  'date_added',
                  'status'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the BoxCategory for this model.
     *
     * @return App\Models\BoxCategory
     */
    public function BoxCategory()
    {
        return $this->belongsTo('App\Models\BoxCategory','box_category_id','id');
    }

    /**
     * Get the boxSubCategory for this model.
     *
     * @return App\Models\BoxSubCategory
     */
    public function boxSubCategory()
    {
        return $this->belongsTo('App\Models\BoxSubCategory','box_sub_category_id');
    }

    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    /**
     * Get the boxAssignment for this model.
     *
     * @return App\Models\BoxAssignment
     */
    public function boxAssignment()
    {
        return $this->hasOne('App\Models\BoxAssignment','load_id','id');
    }

    /**
     * Get the ecycleFailedTests for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function ecycleFailedTests()
    {
        return $this->hasMany('App\Models\EcycleFailingTest','load_id','id');
    }

    /**
     * Get the escalateItems for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function escalateItems()
    {
        return $this->hasMany('App\Models\EscalateItem','load_id','id');
    }

    /**
     * Get the incomingProcessingActivityLogs for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function incomingProcessingActivityLogs()
    {
        return $this->hasMany('App\Models\IncomingProcessingActivityLog','load_id','id');
    }

    /**
     * Get the loadsTimestampLog for this model.
     *
     * @return App\Models\LoadsTimestampLog
     */
    public function loadsTimestampLog()
    {
        return $this->hasOne('App\Models\LoadsTimestampLog','load_id','id');
    }

    /**
     * Get the npnItems for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function npnItems()
    {
        return $this->hasMany('App\Models\NpnItem','load_id','id');
    }

    /**
     * Get the rtiItems for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function rtiItems()
    {
        return $this->hasMany('App\Models\RtiItem','load_id','id');
    }

    /**
     * Get the wheItems for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function wheItems()
    {
        return $this->hasMany('App\Models\WheItem','load_id','id');
    }

    public static function getAllLoads()
    {
        $result = Loads::from('loads as l')
            ->select(
                'l.id',
                'l.source',
                'l.box_number'
            )
            ->get()
        ;
        return $result;
    }

}
