<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbayApiCredentials extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ebay_api_credentials';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'version',
                  'global_id',
                  'app_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the global for this model.
     *
     * @return App\Models\Global
     */
    public function global()
    {
        return $this->belongsTo('App\Models\Global','global_id');
    }

    /**
     * Get the app for this model.
     *
     * @return App\Models\App
     */
    public function app()
    {
        return $this->belongsTo('App\Models\App','app_id');
    }



}
