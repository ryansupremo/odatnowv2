<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'sku',
        'user',
        'reason',
        'notes',
        'dismiss',
        'snooze'
    ];

    public static function notifications($product)
    {
        $data = Notifications::from('notifications as n')
            ->select(
                'n.id',
                'n.product_id',
                'n.sku',
                'n.user',
                'n.reason',
                'n.notes',
                'n.dismiss',
                'n.snooze',
                'n.date_added',
                'n.date_updated',
                'users.name'
            )
            ->leftJoin('users', function($leftJoin)
            {
                $leftJoin->on('users.id', '=', 'n.user');
            })
            ->where('n.product_id', '=', $product->product_id)
            ->orderBy('date_added', 'DESC')
            ->paginate($_ENV['PAGINATE']);
        ;
        return $data;
    }

    public static function alerts($product)
    {
        $count = Notifications::from('notifications as n')
            ->where('n.product_id', '=', $product->product_id)
            ->where('n.dismiss', '!=', 'on')
            ->where('n.snooze', '!=', 'on')
            ->count();
        return $count;
    }
}
