<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleShoppingCsvFiles extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'google_shopping_csv_files';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'success_file',
                  'failed_file',
                  'date_created'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    

    /**
     * Set the date_created.
     *
     * @param  string  $value
     * @return void
     */
    public function setDateCreatedAttribute($value)
    {
        $this->attributes['date_created'] = !empty($value) ? \DateTime::createFromFormat('[% date_format %]', $value) : null;
    }

    /**
     * Get date_created in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getDateCreatedAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('j/n/Y');
    }

}
