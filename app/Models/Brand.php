<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'brand';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'brand_id',
                  'name'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the brand for this model.
     *
     * @return App\Models\Brand
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand','brand_id');
    }

    public static function getBrand($brand)
    {
        $data = Store::from('brand as b')
            ->select(
                'b.id',
                'b.brand_id',
                'b.name'
            )
            ->where('b.name', '=', $brand)
            ->take(1)
            ->get();
        return $data;
    }

}
