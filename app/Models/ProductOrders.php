<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOrders extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'orders_id',
                  'product_order_id',
                  'order_id',
                  'product_id',
                  'order_address_id',
                  'name',
                  'sku',
                  'type',
                  'base_price',
                  'price_ex_tax',
                  'price_inc_tax',
                  'price_tax',
                  'base_total',
                  'total_ex_tax',
                  'total_inc_tax',
                  'total_tax',
                  'weight',
                  'quantity',
                  'base_cost_price',
                  'cost_price_inc_tax',
                  'cost_price_ex_tax',
                  'cost_price_tax',
                  'is_refunded',
                  'quantity_refunded',
                  'refund_amount',
                  'return_id',
                  'wrapping_name',
                  'base_wrapping_cost',
                  'wrapping_cost_ex_tax',
                  'wrapping_cost_inc_tax',
                  'wrapping_cost_tax',
                  'wrapping_message',
                  'quantity_shipped',
                  'event_name',
                  'event_date',
                  'fixed_shipping_cost',
                  'ebay_item_id',
                  'ebay_transaction_id',
                  'option_set_id',
                  'parent_order_product_id',
                  'is_bundled_product',
                  'bin_picking_number',
                  'external_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Order for this model.
     *
     * @return App\Models\Order
     */
    public function Order()
    {
        return $this->belongsTo('App\Models\Order','orders_id','id');
    }

    /**
     * Get the productOrder for this model.
     *
     * @return App\Models\ProductOrder
     */
    public function productOrder()
    {
        return $this->belongsTo('App\Models\ProductOrder','product_order_id');
    }

    /**
     * Get the order for this model.
     *
     * @return App\Models\Order
     */
    public function orders()
    {
        return $this->belongsTo('App\Models\Order','order_id');
    }

    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    /**
     * Get the orderAddress for this model.
     *
     * @return App\Models\OrderAddress
     */
    public function orderAddress()
    {
        return $this->belongsTo('App\Models\OrderAddress','order_address_id');
    }

    /**
     * Get the return for this model.
     *
     * @return App\Models\Return
     */
    public function return()
    {
        return $this->belongsTo('App\Models\Return','return_id');
    }

    /**
     * Get the ebayItem for this model.
     *
     * @return App\Models\EbayItem
     */
    public function ebayItem()
    {
        return $this->belongsTo('App\Models\EbayItem','ebay_item_id');
    }

    /**
     * Get the ebayTransaction for this model.
     *
     * @return App\Models\EbayTransaction
     */
    public function ebayTransaction()
    {
        return $this->belongsTo('App\Models\EbayTransaction','ebay_transaction_id');
    }

    /**
     * Get the optionSet for this model.
     *
     * @return App\Models\OptionSet
     */
    public function optionSet()
    {
        return $this->belongsTo('App\Models\OptionSet','option_set_id');
    }

    /**
     * Get the parentOrderProduct for this model.
     *
     * @return App\Models\ParentOrderProduct
     */
    public function parentOrderProduct()
    {
        return $this->belongsTo('App\Models\ParentOrderProduct','parent_order_product_id');
    }

    /**
     * Get the external for this model.
     *
     * @return App\Models\External
     */
    public function external()
    {
        return $this->belongsTo('App\Models\External','external_id');
    }



}
