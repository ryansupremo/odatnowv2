<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompatiblesManualTypes extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compatibles_manual_types';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'manual_type'
    ];

    public static function getManualTypes()
    {
        $data = CompatiblesManualTypes::from('compatibles_manual_types as t')
            ->select(
                't.id',
                't.manual_type'
            )
            ->get();

        return $data;
    }
}
