<?php
namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\DescriptionTemplate;
use App\Models\Categories;
use App\Models\GiftWrappingOptionsList;
use App\Models\MetaKeywords;
use App\Models\CustomUrl;
use App\Models\CustomFields;
use App\Models\BulkPricingRules;
use App\Models\Images;
use App\Models\GoogleMerchantCenterCsv;
use App\Models\TimestampLogs;
use App\Http\Controllers\Products\NotificationController;
use App\Services\ProductsService;
use App\Services\BigCommerceApi;

class NewProductController extends Controller
{
    public function saveAction(ProductsService $productService, BigCommerceApi $bc, Request $request)
    {
        /**
         * Test values. These would be in a GET or POST
         */
        //$id = "15326"; // missing name, price, upc, custom field 5 (6)
        $id = "15311";

        die; // this function is disabled for now until more datapoints are defined.

        /**
         * End Test Values
         */

        $product = Products::find($id);

        $child = "1"; // default to child
        $product_id = $id;
        $check = Products::checkIfChild($product->id);
        foreach ($check as $c) {
            if (is_null($c->primary_product_id)) {
                $child = "0"; // this is not a child product
            } else {
                if ($c->primary_product_id == $id) {
                    $child = "0";
                } else {
                    $child = "1";
                    $product_id = $c->primary_product_id;
                }
            }
        }

        $product2 = Products::find($product_id);

        $asin_upc = CustomFields::getCustomFieldsByField($product2, 'ASIN_UPC');
        if (!empty($asin_upc)) {
            $asin_upc_value = $asin_upc['value'];
        }
        if ($asin_upc_value == "") {
            $asin_upc_value = "-";
        }

        $condition = $product->product_condition;

        $store = $productService->getStore();
        $brand = $productService->getBrand($product->brand_name);
        $defaultCategory = $productService->getDefaultCategory();
        $brandCategory = $productService->getBrandCategory($product->brand_name);

        // Store object
        foreach ($store as $s) {
            $token = $s->token;
            $clientID = $s->client_id;
            $storeID = $s->store_id;
        }

        // Brand object
        foreach ($brand as $b) {
            $brandID = $b->brand_id;
        }

        // DefaultCategory object
        $parentID = "";
        $default_category_id = "";

        foreach ($defaultCategory as $d) {
            $parentID = $d->parent_id;
            $default_category_id = $d->category_id;

            /*
            is_default
            sort_order
            category_id
            name
            description
            page_title
            parent_id
            */
        }

        if ($default_category_id < 0) {
            print "Error: the default category ID is blank.";
            die;
        }

        if ($parentID < 0) {
            print "Error: the parentID is blank.";
            die;
        }

        // BrandCategory object
        $brandCatFound = "0";
        if (is_object($brandCategory)) {
            foreach ($brandCategory as $b) {
                $brandCatFound = "1";
                $categoryBrandId = $b->category_id;

                /*
                is_default
                sort_order
                category_id
                name
                description
                page_title
                parent_id
                */
            }
        }

        /**
         * Create the brand category in BigC if empty
         */
        $categoryCreateResult = "";
        $brandCatFound = 0;
        if ($brandCatFound == "0") {
            $createCategoryUrl = 'https://api.bigcommerce.com/stores/' . $storeID . '/v3/catalog/categories';
            $createCategoryPost = json_encode(
                array(
                    'parent_id' => $parentID,
                    'name' => $product->brand_name,
                    'sort_order' => 0
                )
            );
            $categoryCreateResult = json_decode($bc->PostAction($clientID, $token, $createCategoryUrl, $createCategoryPost));
        }

        //$site = 'RemoteControlDepot.com';

        $noBattery = false;
        $noBatteryCover = explode("_", $product->sku);
        if($noBatteryCover[count($noBatteryCover)-1] == "NBC"){
            $noBattery = true;
        }

        $template = DescriptionTemplate::find(2);
        $description = $template->template;

        $nbc = "";
        if($noBattery == true){
            $nbc .= '<h2 style="text-align: center; color:red;"><strong>No Battery Cover Included, use your old one and SAVE!</strong></h2>';
            $toReplace = array('{NBC}');
            $replaceWith = array($nbc);
            $description = str_replace($toReplace, $replaceWith, $description);
        }else{
            $toReplace = array('{NBC}');
            $replaceWith = array($nbc);
            $description = str_replace($toReplace, $replaceWith, $description);
        }

        $nbcProductName = "";
        $nbcUrlExt = "";
        if($noBattery == true){
            $nbcProductName = " No Battery Cover";
            $nbcUrlExt = "_NBC";
        }

        if($condition == 'Refurbished'){
            $productName = $product->brand_name . " " . $product->mpn . " Original Replacement Remote Control$nbcProductName";
            $urlExt = "Original$nbcUrlExt";
        }else if($condition == 'New'){
            $productName = $product->brand_name . " " . $product->mpn . " Genuine Replacement Remote Control$nbcProductName";
            $urlExt = "Genuine$nbcUrlExt";
        }else if($condition == 'Used'){
            $productName = $product->brand_name . " " . $product->mpn . " OEM Replacement Remote Control$nbcProductName";
            $urlExt = "OEM$nbcUrlExt";
        }

        $warrantyTemplate = 'Remote Control';

        $categories = array($default_category_id, $categoryBrandId);
        $mpnForUrl = strtoupper(str_replace(' ','-', trim(($product->mpn != '[add]' ? $product->mpn : '-'))));
        $mpnForUrl = preg_replace('/[^a-zA-Z0-9\']/', '_', $mpnForUrl);
        $bulletPoints = 'Genuine OEM Remote Control | Easy Operation - Needs no programming | This remote IS Original-OEM, NOT a "Generic", "Fit For" or a "Replacement For" remote. | Promptly shipped in professional packaging. | Batteries/Manual Not included.';
        $additionalInfo = 'Genuine ' . $product->brand_name . ' ' . $product->mpn . ' Remote Control. Buy with confidence. This is not a  "Fit For", "Replacement For", "Generic" or "Substitute" Remote. These terms mean it is likely a replica or knock off of inferior quality.';
        $conditionNotes = 'Lifetime Warranty - Buy with Confidence this is a Genuine OEM Remote, NOT a  "Fit For" - "Replacement For" - "Generic" - "Substitute" or "Unbranded" Remote. These terms mean it is likely a replica or knock off of inferior quality.';

        $asin = $product->amazon_flag_no_asin;
        if ($product->amazon_flag_no_asin != '[add]') {
            $asin = '-';
        }
        if (is_null($product->amazon_flag_no_asin)) {
            $asin = '-';
        }

        $metaKeywords = array(
            $product->brand_name,
            $product->mpn . " Remote",
            "Control",
            "Programming",
            "Codes",
            "Manual",
            "Replacement"
        );

        $url = 'https://api.bigcommerce.com/stores/' . $storeID . '/v3/catalog/products';

        var_dump($product->name);

        $post = json_encode(
            array(
                'name' => $product->name,
                'sku' => $product->sku,
                'description' => $description,
                'weight' => 0,
                'width' => 0,
                'depth' => 0,
                'height' => 0,
                'type' => 'physical',
                'price' => $product->price,
                'tax_class_id' => 0,
                'brand_id' => $brandID,
                'inventory_level' => $product->inventory_level,
                'inventory_warning_level' => $product->inventory_warning_level,
                'inventory_tracking' => 'product',
                'is_free_shipping' => false,
                'is_visible' => true,
                'bin_picking_number' => $product->bin_picking_number,
                'availability' => 'available',
                'categories' => $categories,
                'condition' => $condition,
                'is_condition_shown' => true,
                'warranty' => $warrantyTemplate,
                'search_keywords' => 'Genuine ' . $product->brand_name . ' ' . $product->mpn . ' Remote Control Controller Clicker ' . $product->sku . '',
                'page_title' => $product->brand_name . ' ' . $product->mpn . ' Replacement Remote Control Lifetime Warranty & Free Shipping !',
                'mpn' => $product->mpn,
                'upc' => $product->upc,
                'gps_ena' => true,
                'custom_url' => array(
                    'url' => str_replace(' ','', '/' . $mpnForUrl . '-' . $product->brand_name . '-Remote-Control-'.$urlExt),
                    'is_customized' => true
                ),
                'custom_fields' => array(
                    array(
                        'name' => 'Bullet Points',
                        'value' => $bulletPoints
                    ),
                    array(
                        'name' => 'Additional Info',
                        'value' => $additionalInfo
                    ),
                    array(
                        'name' =>  'ConditionNotes',
                        'value' => $conditionNotes
                    ),
                    array(
                        'name' => 'sku',
                        'value' => $product->sku
                    ),
                    array(
                        'name' => 'ASIN',
                        'value' => $asin
                    ),
                    array(
                        'name' => 'ASIN_UPC',
                        'value' => $product->upc
                    )
                ),

                'meta_description' => 'Buy this ' . $product->brand_name . ' ' . $product->mpn . ' with confidence today. Free Shipping - Lifetime Warranty - 30 days satisfaction guaranteed! We do remotes RIGHT!',
                'meta_keywords' => $metaKeywords

            )
        );

        $return = json_decode($bc->PostAction($clientID, $token, $url, $post));
        print "<pre>";
        print_r($return);
        print "</pre>";

        /**
         * ERROR! ERROR! ERROR! ERROR! ERROR! ERROR! ERROR! ERROR! ERROR! ERROR!
         */

        /*
         *
         * stdClass Object
        (
            [status] => 422
            [title] => Invalid field(s): name, price, upc, custom_fields.5.value
            [type] => https://developer.bigcommerce.com/api-docs/getting-started/api-status-codes
            [errors] => stdClass Object
                (
                    [name] => name must be a string
                    [price] => price must be a float number
                    [upc] => upc must be a string
                    [custom_fields.5.value] => Custom Field Value must be a string
                )

        )
         *
         */

        $dateTime = date('Y-m-d H:i:s');
        $dateTimeUnix = strtotime(date('Y-m-d H:i:s'));

        if(isset($return['data']['id'])){
            $data = $return['data'];
            $product->product_id = $data['id'];
            $product->name = $data['name'];
            $product->type = $data['type'];
            $product->mpn = $data['variants'][0]['mpn'];
            $product->description = $data['description'];
            $product->weight = $data['weight'];
            $product->width = $data['width'];
            $product->depth = $data['depth'];
            $product->height = $data['height'];
            $product->price = $data['price'];
            $product->cost_price = $data['cost_price'];
            $product->retail_price = $data['retail_price'];
            $product->sale_price = $data['sale_price'];
            $product->tax_class_id = $data['tax_class_id'];
            $product->product_tax_code = $data['product_tax_code'];
            $product->brand_id = $data['brand_id'];
            $product->inventory_level = $data['inventory_level'];
            $product->inventory_warning_level = $data['inventory_warning_level'];
            $product->inventory_tracking = $data['inventory_tracking'];
            $product->fixed_cost_shipping_price = $data['fixed_cost_shipping_price'];
            $product->is_free_shipping = $data['is_free_shipping'];
            $product->is_visible = $data['is_visible'];
            $product->is_featured = $data['is_featured'];
            $product->warranty = $data['warranty'];
            $product->bin_picking_number = $data['bin_picking_number'];
            $product->layout_file = $data['layout_file'];
            $product->upc = $data['upc'];
            $product->search_keywords = $data['search_keywords'];
            $product->availability = $data['availability'];
            $product->availability_description = $data['availability_description'];
            $product->gift_wrapping_options_type = $data['gift_wrapping_options_type'];
            $product->sort_order = $data['sort_order'];
            $product->product_condition = $data['condition'];
            $product->is_condition_shown = $data['is_condition_shown'];
            $product->order_quantity_minimum = $data['order_quantity_minimum'];
            $product->order_quantity_maximum = $data['order_quantity_maximum'];
            $product->page_title = $data['page_title'];
            $product->meta_description = $data['meta_description'];
            $product->view_count = $data['view_count'];
            $product->preorder_release_date = strtotime($data['preorder_release_date']);
            $product->preorder_message = $data['preorder_message'];
            $product->is_preorder_only = $data['is_preorder_only'];
            $product->is_price_hidden = $data['is_price_hidden'];
            $product->price_hidden_label = $data['price_hidden_label'];
            $product->calculated_price = $data['calculated_price'];
            $product->date_created = strtotime($data['date_created']);
            $product->date_modified = strtotime($data['date_modified']);
            $product->adwords_on = false;
            $product->google_shopping_on = false;
            $product->is_compatible = false;
            $product->status = "For Review";
            $product->last_inventory = $dateTime;
            $product->last_updated = $dateTimeUnix;
            $product->last_optimized = new \DateTime($dateTime);
            $product->image_age = 0;
            $product->save();

            Categories::insertCategories($product, $data['categories']);
            GiftWrappingOptionsList::insertGiftWrappingOptionList($product, $data['gift_wrapping_options_list']);
            MetaKeywords::insertMetaKeywords($product, $data['meta_keywords']);
            CustomUrl::insertCustomUrl($product, $data['custom_url']);
            CustomFields::insertCustomFields($product, $data['custom_fields']);
            BulkPricingRules::insertBulkPricingRules($product, $data['bulk_pricing_rules']);

            // Images
            $images = Images::getImageDetails($product_id);

            if ($images) {
                foreach ($images as $i) {
                    $imageCreateUrl = 'https://api.bigcommerce.com/stores/' . $storeID . '/v3/catalog/products/' . $data['id'] . '/images';
                    $postCreateUrl = json_encode(array(
                        'is_thumbnail' => $i->is_thumbnail,
                        'sort_order' => $i->sort_order,
                        'description' => '',
                        'image_url' => $i->url_zoom,
                        'image_file' => $i->image_file_name
                    ));
                    $responseCreateImage = $bc->PostAction($clientID, $token, $imageCreateUrl, $postCreateUrl);
                    if(isset($responseCreateImage['data']['id'])){
                        // We will now change what is set in the image with what big C returns
                        $image = Images::find($i->id);
                        $image->image_id = $responseCreateImage['data']['id'];
                        $image->product_id = $product_id;
                        $image->is_thumbnail = $responseCreateImage['data']['is_thumbnail'];
                        $image->sort_order = $responseCreateImage['data']['sort_order'];
                        $image->description = $responseCreateImage['data']['description'];
                        $image->image_file = $responseCreateImage['data']['image_file'];
                        $image->url_zoom = $responseCreateImage['data']['url_zoom'];
                        $image->url_standard = $responseCreateImage['data']['url_standard'];
                        $image->url_thumbnail = $responseCreateImage['data']['url_thumbnail'];
                        $image->url_tiny = $responseCreateImage['data']['url_tiny'];
                        $image->date_modified = strtotime($responseCreateImage['data']['date_modified']);
                        $image->is_pending = false;
                        $image->save();
                    }

                }
            }

            $this->sendToFinal($product, $product2, $productService, $bc);

            // Record timestamp log TBD
            $this->recordTimestampLogs($product2, $request, $type);

        }
        return true;
    }

    public function sendToFinal($product, $product2, $productService, $bc)
    {
        $asin_upc = CustomFields::getCustomFieldsByField($product2, 'ASIN_UPC');
        $customUrl = CustomUrl::getProductUrl($product2);
        $custom_url = "";
        foreach ($customUrl as $c) {
            $custom_url = $c->url;
        }

        if (!empty($asin_upc)) {
            $asin_upc_value = $asin_upc['value'];
        }
        if ($asin_upc_value == "") {
            $asin_upc_value = "-";
        }

        $google = GoogleMerchantCenterCsv::getGoogleCsv($product);
        if ($google) {
            foreach ($google as $g) {
                $g->product_id = $product->id;
                $g->sku = $product->sku;
                $g->brand = $product->brand;
                $g->mpn = $product->mpn;
                $g->asin_upc = $asin_upc_value;
                $g->product_name = $product->name;
                $g->product_type = "P";
                $g->product_description = $product->description;
                $g->product_availability = $product->availability;
                $g->category = implode(';', $this->getProductCategories($product, $product2, $productService, $bc));
                $g->product_condition = $product->product_condition;
                $g->product_url = 'http://www.remotecontroldepot.com' . $custom_url;
                $g->gps_enabled = 'Y';
                $g->gps_category = 'Shop by Brand';
                $g->ebay_eligible = $product->ebay_eligible != null ? true : false;
                $g->amazon_eligible = $product->amazon_eligible != null ? true : false;
                $g->amazon_pricing = $product->amazon_flag_pricing != null ? true : false;
                $g->amazon_no_asin = $product->amazon_flag_no_asin != null ? true : false;
                $g->amazon_restricted = $product->amazon_flag_restricted != null ? true : false;
                $g->amazon_needs_support_call = $product->amazon_flag_needs_support_call != null ? true : false;
                $g->amazon_other = $product->amazon_flag_other != null ? true : false;
                $g->save();
            }
        } else {
            // new GoogleMerchantCenterCsv
            $g = new GoogleMerchantCenterCsv();
            $g->sku = $product->sku;
            $g->brand = $product->brand;
            $g->mpn = $product->mpn;
            $g->asin_upc = $asin_upc_value;
            $g->product_name = $product->name;
            $g->product_type = "P";
            $g->product_description = $product->description;
            $g->product_availability = $product->availability;
            $g->category = implode(';', $this->getProductCategories($product, $product2, $productService, $bc));
            $g->product_condition = $product->product_condition;
            $g->product_url = 'http://www.remotecontroldepot.com' . $custom_url;
            $g->gps_enabled = 'Y';
            $g->gps_category = 'Shop by Brand';
            $g->ebay_eligible = $product->ebay_eligible != null ? true : false;
            $g->amazon_eligible = $product->amazon_eligible != null ? true : false;
            $g->amazon_pricing = $product->amazon_flag_pricing != null ? true : false;
            $g->amazon_no_asin = $product->amazon_flag_no_asin != null ? true : false;
            $g->amazon_restricted = $product->amazon_flag_restricted != null ? true : false;
            $g->amazon_needs_support_call = $product->amazon_flag_needs_support_call != null ? true : false;
            $g->amazon_other = $product->amazon_flag_other != null ? true : false;
            $g->save();
        }

        $product->last_updated = time();
        $product->compatible_status = 'NA';
        $product->adwords_status = 'NA';
        $product->save();
        return true;
    }

    public function getProductCategories($product, $product2, $productService, $bc)
    {
        $store = $productService->getStore();
        $token = "";
        $clientID = "";
        $storeID = "";

        foreach ($store as $s) {
            $token = $s->token;
            $clientID = $s->client_id;
            $storeID = $s->store_id;
        }

        $data = array();
        $post = array();

        $categories = Categories::getCategories($product2);
        if (is_object($categories)) {
            foreach ($categories as $category) {
                $url = 'https://api.bigcommerce.com/stores/' . $storeID . '/v3/catalog/categories/'.$category->category;
                $response = json_decode($bc->GetAction($clientID, $token, $url, $post), true);
            }

            if(isset($response['data']['id'])){
                $data[] = $response['data']['name'];
            }
        }

        return $data;
    }

    public function recordTimestampLogs($product, $request, $type)
    {
        $dateNow = date('Y-m-d H:i:s');
        $dateNowUnix = strtotime($dateNow);

        $productData = array(
            'id' => $product->id,
            'productId' => $product->product_id,
            'name' => $product->name,
            'sku' => $product->sku,
            'mpn' => $product->mpn,
            'description' => $product->description,
            'price' => $product->price,
            'brandId' => $product->brand_id,
            'inventoryLevel' => $product->inventory_level,
            'inventoryWarningLevel' => $product->inventory_warning_level,
            'binPickingNumber' => $product->bin_picking_number,
            'upc' => $product->upc
        );

        $user = $request->user();

        $log = new TimestampLogs();
        $log->product_id = $product->product_id;
        $log->sku = $product->sku;
        $log->type = $type;
        $log->sub_type = "N/A";
        $log->old_data = json_encode($productData);
        $log->data = json_encode($productData);
        $log->timestamp_date = new \DateTime($dateNow);
        $log->timestamp_unix = $dateNowUnix;
        $log->user = $user->name;
        $log->update_status = "success";
        $log->save();

        return true;
    }
}
