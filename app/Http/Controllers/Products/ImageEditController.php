<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\ProductImagesDraft;

class ImageEditController extends Controller
{
    public function testAction($id)
    {
        $page_title = "Test Upload Page";

        return view('pages.images.upload', [
            'page_title' => $page_title,
            'id' => $id,
        ]);
    }

    public function uploadImageAction(Request $request)
    {
        // return response()->json([
        //     'request' => $request->all()
        // ]);
        /**
         * The following fields should be posted to the controller:
         * id : hidden : this is product.id
         * image: file
         * type: string : options (main, secondary)
         */

        if (($request->request->get('type') != "main") && ($request->request->get('type') != "secondary")) {
            return response()->json([
                'success' => 502,
                'message' => 'The type is not valid.',
            ]);
        }

        if ($request->request->get('type') == "") {
            return response()->json([
                'success' => 502,
                'message' => 'The type is missing.',
            ]);
        }

        $id = $request->request->get('id');
        $product = Products::find($id);
        $query = ProductImagesDraft::locateImage($id);
        $user = $request->user();

        // handle image upload
        // $file = $request->files->get('image'); old line
        $file = $request->file;

        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $uploadDir = public_path('products');

        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }

        $found = 0;
        foreach ($query as $q) {
            $found = 1;
        }

        if ($found == "0") {
            $type = "main";
        } else {
            $type = "secondary";
        }

        if ($file->move($uploadDir, $fileName)) {
            /**
             * add image to database here
             */
            $image = new ProductImagesDraft();
            $image->product_id = $product->id;
            $image->image = $fileName;
            $image->type = $type;
            $image->userID = $user->id;
            $image->status = "draft";
            $image->date_added = new \DateTime();
            $image->date_updated = new \DateTime();
            $image->save();

            return response()->json([
                'success' => 200,
                'message' => 'The image was saved.',
                'image' => $image->image,
            ]);
        }

        return response()->json([
            'success' => 502,
            'message' => 'The image did not save.',
        ]);
    }

    public function getAllImagesAction($id, Request $request)
    {
        $data = ProductImagesDraft::getAllImagesById($id);
        return response()->json([
            'success' => 200,
            'data'=>  $data
            ]);
    }

    public function getSingleImageAction($id, Request $request)
    {
        $data = ProductImagesDraft::find($id); // product_images_drafts.id
        return response()->json($data);
    }

    public function deleteSingleImageAction($id)
    {
        $data = ProductImagesDraft::find($id); // product_images_drafts.id
        $fileName = public_path('products') . '/' . $data->image;
        unlink($fileName);
        $data->delete();
        return response()->json([
            'success' => 200,
            'message' => 'The image was deleted.',
        ]);
    }

    public function editSingleImageTypeAction($id, $type, Request $request)
    {
        if (($type != "main") && ($type != "secondary")) {
            return response()->json([
                'success' => 502,
                'message' => 'The type is not valid.',
            ]);
        }

        if ($type == "main") {
            // make all secondary
            $data = ProductImagesDraft::find($id);
            ProductImagesDraft::setAllSecondary($data->product_id);
        }

        $data = ProductImagesDraft::find($id);
        $data->type = $type;
        $data->save();
        return response()->json([
            'success' => 200,
            'message' => 'The image type was updated.',
        ]);
    }
}
