<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Loads;

class AddProductController extends Controller
{
    public function newAction()
    {
        $page_title = "New Product";
        $loads = Loads::getAllLoads();

        return view('pages.products.new', [
            'page_title' => $page_title,
            'loads' => $loads,
        ]);
    }
}
