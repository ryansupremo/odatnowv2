<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;

class LocationController extends Controller
{
    public function updateLocationAction($id, Request $request)
    {
        if ($request->request->get('location') == "") {
            return response()->json([
                'success' => 502,
                'message' => 'The location is missing.',
            ]);
        }

        $product = Products::find($id);
        $product->location = $request->request->get('location');
        $product->save();

        return response()->json([
            'success' => 200,
            'message' => 'The location was updated.',
        ]);
    }

    public function updateBinAction($id, Request $request)
    {
        if ($request->request->get('bin_picking_number') == "") {
            return response()->json([
                'success' => 502,
                'message' => 'The bin is missing.',
            ]);
        }

        $product = Products::find($id);
        $product->bin_picking_number = $request->request->get('bin_picking_number');
        $product->save();

        return response()->json([
            'success' => 200,
            'message' => 'The bin was updated.',
        ]);
    }

    public function updateQohAction($id, Request $request)
    {
        if ($request->request->get('inventory_level') == "") {
            return response()->json([
                'success' => 502,
                'message' => 'The QOH is missing.',
            ]);
        }

        $product = Products::find($id);
        $product->inventory_level = $request->request->get('inventory_level');
        $product->save();

        return response()->json([
            'success' => 200,
            'message' => 'The QOH was updated.',
        ]);
    }
}
