<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function showAction(Request $request)
    {
        $user = $request->user();

        return view('pages.users.profile.show', [
            'user' => $user,
        ]);
    }

    public function editAction(Request $request)
    {
        $user = $request->user();

        return view('pages.users.profile.edit', [
            'user' => $user,
        ]);
    }

    public function updateAction(Request $request)
    {
        $user = $request->user();
        $id = $user->id;

        // check for duplicate
        $users = User::where('email', '=', $request->request->get('email'))->get();
        $found = "0";
        foreach ($users as $user) {
            $found++;
        }

        if ($found > "1") {
            return redirect()->route('profile.edit')->with('message', "The email {$request->request->get('email')} is already registered with another user.");
        }

        // save user
        $user = User::find($id);
        $user->name = $request->request->get('name');
        $user->email = $request->request->get('email');
        if ($request->request->get('password') != "") {
            $user->password = bcrypt($request->request->get('password'));
        }
        $user->save();

        return redirect()->route('dashboard')->with('message', "Your profile was updated.");
    }
}
