<?php

namespace App\Http\Controllers\Bulk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BulkController extends Controller
{
    public function listAction()
    {
        return view('pages.bulk.list');
    }
}
