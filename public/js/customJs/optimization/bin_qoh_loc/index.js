$(document).ready(function () {

    const binInput = $('#binInput')
    const bin = $('#bin')
    const id = $('#id').val()
    let oldBin = $('#bin').text()

    binInput.hide()
    bin.click(function () {
        bin.hide()
        binInput.show()
        binInput.focus()
    })
    function changeBin(binValue) {
        $.ajax({
            url: `/products/optimization/bin/${id}`,
            data: { bin_picking_number: binValue },
            type: "POST",
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
        })
            .then(response => {
                if (response.success === 200) {
                    $('.alert.alert-success').text(response.message).show().fadeOut(4000)
                } else {
                    $('.alert.alert-danger').text(response.message).show().fadeOut(4000)
                    bin.text(oldBin).show()
                }
            })
    }

    binInput.focusout(function () {
        if (binInput.val() !== ""){
           bin.text(binInput.val()).show()
            $(this).hide()
            changeBin(binInput.val())
        } else {
            $(this).hide()
            bin.text(oldBin).show()
        }
    })
    binInput.keyup(function (e) {
        if (e.keyCode === 13) {
            if (binInput.val() !== ""){
                bin.text(binInput.val()).show()
                $(this).hide()
                changeBin(binInput.val())
            } else {
                $(this).hide()
                binInput.text(oldBin).show()
            }
        }
    })

    // QOH update
    // inventory_level
    const qoh = $('#qoh')
    const qohInput = $('#qohInput')
    const oldQoh = qoh.text()

    qohInput.hide()
    qoh.click(function (e) {
        $(this).hide()
        qohInput.show()
        qohInput.focus()
    })
    function changeQoh(qohValue){
        $.ajax({
            url: `/products/optimization/qoh/${id}`,
            data: {inventory_level: qohValue},
            type: "POST",
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('input[name="csrf-token"]').val(),
            }
        })
        .then(response => {
            if (response.success === 200){
                $('.alert.alert-success').text(response.message).show().fadeOut(4000)
            } else {
                $('.alert.alert-danger').text(response.message).show().fadeOut(4000)
                qoh.text(oldBin).show()
            }
        })
    }

    qohInput.focusout(function (e) {
        if (qohInput.val() !== ""){
            qoh.text(qohInput.val()).show()
            $(this).hide()
            changeQoh(qohInput.val())
        } else {
            $(this).hide()
            qoh.text(oldQoh).show()
        }
    })
    qohInput.keyup(function (e) {
        if (e.keyCode === 13) {
            if (qohInput.val() !== ""){
                qoh.text(qohInput.val()).show()
                $(this).hide()
                changeBin(qohInput.val())
            } else {
                $(this).hide()
                qohInput.text(oldQoh).show()
            }
        }
    })

//    Location
    const locationInput = $('#locationInput')

    locationInput.hide()
    $('.alert').alert()
    let oldValue = $('#location').text()
    $('#location').click(function(){
        $(this).hide()
        locationInput.show()
        locationInput.focus()
    })
    function changeLocation(location) {
        $.ajax({
            url: `/products/optimization/location/${id}`,
            data: { location: location },
            type: "POST",
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                // "Content-Type": "application/x-www-form-urlencoded"
            },
        })
            .then(res => {
                if (res.success === 200) {
                    $('.alert.alert-success').text(res.message).show().fadeOut(4000)
                } else {
                    $('.alert.alert-danger').text(res.message).show().fadeOut(4000)
                    $('#location').text(oldValue).show()
                }
            })
    }
    locationInput.focusout(function(e) {
        if (locationInput.val() !== '' ) {
            $('#location').text(locationInput.val()).show()
            $(this).hide()
            changeLocation(locationInput.val())
        } else {
            $(this).hide()
            $('#location').text(oldValue).show()
        }
    })
    locationInput.keyup(function(e) {
        if(e.keyCode === 13) {
            if (locationInput.val() !== '' ) {
                $('#location').text(locationInput.val()).show()
                $(this).hide()
                changeLocation(locationInput.val())
            } else {
                $(this).hide()
                $('#location').text(oldValue).show()
            }
        }
    })
})
