$(document).ready(function () {
    let id = $('#id').val()
    let compatiblesID = $('#compatiblesID').val()

    $('#createManual').click(function () {
        let data = {
            id,
            compatiblesID,
            link: $('#link').val(),
            note: $('#note').val(),
            manual_id: $('#manual_id').val()
        }
        if (data.link === ""){
            $('.alert.alert-danger.link').text('Link field is required').show().fadeOut(3000)
        } else if(data.manual_id === "") {
            $('.alert.alert-danger.type').text('Type field is required').show().fadeOut(3000)
        } else {
            createNewManual(id, compatiblesID, data)
        }


    })
    // /products/optimization/compatibles/manuals/delete/{recordID}
    $(document).on('click', '.deleteManual', function () {
        let recordID = $(this).attr('data-manual-id')
        Swal.fire({
            title: 'Are you sure?',
            text: "This item will be removed!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: `/products/optimization/compatibles/manuals/delete/${recordID}`,
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                    },
                    type: "GET",
                    dataType: 'JSON',
                })
                    .then(res => {
                        if (res.success === 200) {
                            $(this).parent().parent().remove()
                            Swal.fire(
                                'Deleted!',
                                'Model has been deleted.',
                                'success'
                            )
                        }
                    })
            }

        })
    })




    function createNewManual(id, compatiblesID, data) {
        $.ajax({
            url: `/products/optimization/compatibles/manuals/save`,
            type: "POST",
            dataType: "JSON",
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val()
            }
        })
            .then(response => {
                $('#link').val('')
                $('#note').val('')
                let data = JSON.parse(response.data)
                $('#manualsList').html('')
                if (response.success === 200){
                    $('.alert.alert-success').text('Manual created').show().fadeOut(3000)
                    $.each(data, function (key, value) {
                        let row = `
                            <tr>
                                <td>${parseInt(key) + parseInt(1)}</td>
                                <td>${value.manual_type}</td>
                                <td>
                                    <a href="${value.link}" target="_blank">
                                        <i class="fas fa-external-link-alt font-size-15 text-primary"></i>
                                    </a>
                                </td>
                                <td>${value.note}</td>
                                <td class="text-center">
                                    <i class="deleteManual cursor-pointer far fa-trash-alt text-danger font-size-15"
                                       data-manual-id="${value.id}"></i>
                                </td>
                            </tr>
                        `
                        $('#manualsList').append(row)
                    })
                }
            }).catch(response => console.log('catch response', response))
    }
})
