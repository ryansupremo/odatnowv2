
$(document).ready(function () {
    //Charts
    const salesChart = $('#salesChart')
    const buyBox = $('#buyBox')
    const optBuyBox = {
        events: false,
        tooltips: {
            enabled: false
        },
        hover: {
            animationDuration: 0
        },
        animation: {
            duration: 1,
            onComplete: function () {
                const chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset, i) {
                    const meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function (bar, index) {
                        const data = dataset.data[index]+"%";
                        ctx.fillText(data, bar._model.x, bar._model.y + 8);
                    });
                });
            },
        },
    };
    const optSales = {
        events: false,
        tooltips: {
            enabled: false
        },
        hover: {
            animationDuration: 0
        },
        animation: {
            duration: 1,
            onComplete: function () {
                const chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset, i) {
                    const meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function (bar, index) {
                        const data = dataset.data[index];
                        ctx.fillText(data, bar._model.x, bar._model.y + 8);
                    });
                });
            },
        },
    };
        let end = moment()
        let start = moment().subtract(6, "month")
        let starMonth = start.format('MMMM')
        let endMonth = end.format('MMMM')
        $('#fromRange').html(starMonth)
        $('#toRange').html(endMonth)
        let monthDifference =  moment(new Date(end)).diff(new Date(start), 'months', true);
        monthDifference = Math.round(monthDifference) +1
        let salesData = []
        let buyData = []
        for ($i = 0; $i < monthDifference; $i++) {
            salesData.push(Math.floor(Math.random(0, 5) * 10))
            buyData.push(Math.floor(Math.random(0, 5) * 10))
        }
        changeChart(start, end, salesData, buyData)
    //Charts end

    const Picker = $('#picker')
    Picker.daterangepicker({
        ranges: {
            '1 Month': [moment().subtract(1, 'month'), moment()],
            '3 Months': [moment().subtract(3, 'month'), moment()],
            '6 Months': [moment().subtract(6, 'month'), moment()],
            '1 Year': [moment().subtract(12, 'month'), moment()],
            'All': [moment().startOf('year'), moment()],
        },
        "alwaysShowCalendars": true,
        "showCustomRangeLabel": true,
        "autoApply": false,
        // "showDropdowns": true,
        // "startDate": moment(),
        // "endDate": moment(),
        "opens": "left",
    }, function(start, end, label) {
        console.log('Details Page Date Range: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        //value in input
        // ajax call will be here and will get data for both charts at the same time for same range

        //dummy data create to display charts only
        const monthDifference =  moment(new Date(end)).diff(new Date(start), 'months', true);
        let salesData = []
        let buyData = []
        for ($i = 0; $i <= monthDifference; $i++) {
            salesData.push(Math.floor(Math.random(0, 5) * 10))
            buyData.push(Math.floor(Math.random(0, 5) * 10))
        }
        let starMonth = start.format('MMMM')
        let endMonth = end.format('MMMM')
        $('#fromRange').html(starMonth)
        $('#toRange').html(endMonth)

        $('#picker').text(start.format('YY/MM/DD') + ' to ' + end.format('YY/MM/DD')).css({'font-size': '0.8rem'})
        changeChart(start, end, salesData, buyData)

    });
    Picker.val(`Date Range `)



    function changeChart(date1, date2, salesData, buyBoxData){
        const dateStart = date1
        const dateEnd = date2
        const Months = [];
        //get all months names in array
        while (dateEnd > dateStart || dateStart.format('YYYY-MM-DD') === dateEnd.format('YYYY-MM-DD')) {
            Months.push(dateStart.format('MMMM'));
            dateStart.add(1,'month');
        }


        let dataSales = {
            labels: Months,
            datasets: [
                {
                    label: "Sales",
                    data: salesData,
                    borderColor: "purple",
                    pointBackgroundColor: 'white',
                    pointBorderWidth: 15,
                },
            ],
        }
        let dataBuyBox = {
            labels: Months,
            datasets: [
                {
                    label: "Buy Box",
                    data: buyBoxData,
                    borderColor: "blue",
                    pointBackgroundColor: 'white',
                    pointBorderWidth: 15,
                },
            ]
        }
        const Sales = new Chart(salesChart, {
            type: 'line',
            data: dataSales,
            options: optSales,
        });
        const Buy = new Chart(buyBox, {
            type: "line",
            data: dataBuyBox,
            options: optBuyBox,
        })

    }


})
