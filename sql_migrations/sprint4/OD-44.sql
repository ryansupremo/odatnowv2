CREATE TABLE `compatibles_manual_types` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `manual_type` VARCHAR(255) NOT NULL , `date_added` DATETIME NULL , `date_updated` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `compatibles_manual_types` (`id`, `manual_type`, `date_added`, `date_updated`) VALUES (NULL, 'Service', '2020-09-05 00:00:00', '2020-09-05 00:00:00'), (NULL, 'Operating', '2020-09-05 00:00:00', '2020-09-05 00:00:00'), (NULL, 'Set-Up', '2020-09-05 00:00:00', '2020-09-05 00:00:00'), (NULL, 'Remote Control', '2020-09-05 00:00:00', '2020-09-05 00:00:00');

CREATE TABLE `products_compatibles_manuals` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `product_id` INT NOT NULL , `compatibles_id` INT NOT NULL , `manual_id` INT NOT NULL , `date_added` DATETIME NULL , `date_updated` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `products_compatibles_manuals` ADD `link` LONGTEXT NULL AFTER `manual_id`, ADD `note` LONGTEXT NULL AFTER `link`;

ALTER TABLE `products_compatibles_manuals` DROP `manual_id`;

ALTER TABLE `products_compatibles_manuals` ADD `manual_id` INT NULL AFTER `note`;
